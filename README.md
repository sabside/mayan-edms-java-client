# Mayan EDMS Java Client

This project is a Java client for Mayan EDMS. It's currently a work in progress, and contributions are welcome. The client is designed to interact with Mayan EDMS's API, providing a Java interface to work with documents and cabinets within Mayan EDMS.

## Configuration

Before using the client, you need to configure it with your Mayan EDMS instance details. Here is how you can set up the configuration in a Spring Boot application:

```java
@Configuration
public class ApplicationConfig {
    @Autowired
    private RestTemplate restTemplate;

    @Value("${mayan.edms.host}")
    private String host;

    @Value("${mayan.edms.password}")
    private String password;

    @Value("${mayan.edms.username}")
    private String username;

    @Bean
    public MayanaEDMSConfig mayanConfig() {
        return MayanaEDMSConfig.builder()
            .host(host)
            .password(password)
            .username(username)
            .build();
    }

    @Bean
    public DocumentService documentService() {
        return new DocumentService(mayanConfig(), restTemplate);
    }

    @Bean
    public CabinetService cabinetService() {
        return new CabinetService(mayanConfig(), restTemplate);
    }
}
```

## Usage Example
Here's an example of how you might use the client to archive a tenant's statement:

```java
@Service
@Slf4j
@RequiredArgsConstructor
public class StatementArchiveService {
    private final DocumentService documentService;
    private final CabinetService cabinetService;
    private final Retry retry;

    @Value("${maya.edms.document.type}")
    private Long documentType;

    @Value("${maya.edms.document.parent-cabinet}")
    private int parentCabinet;

    public Document archiveStatement(Tenant tenant, File statement) {
        Supplier<Document> docCreateSupplier = () -> {
            return doArchiving(tenant, statement);
        };

        Document createdDocument = Decorators.ofSupplier(docCreateSupplier)
            .withRetry(retry)
            .get();

        return createdDocument;
    }

    private Document doArchiving(Tenant tenant, File statement) {
        log.info("Archiving statement");

        Document createdDocument = documentService.uploadDocument(statement, documentType);
        log.info("Statement archived: {}", createdDocument);

        Cabinet tenantCabinet = cabinetService.list()
            .stream()
            .filter(c->c.getLabel().equals(String.format("%s %s", tenant.getFirstname(), tenant.getLastname())))
            .findFirst()
            .orElseGet(() -> {
                log.info("Creating cabinet for tenant: {}", tenant.getFirstname());
                Cabinet cabinet = cabinetService.create(String.format("%s %s", tenant.getFirstname(), tenant.getLastname()), parentCabinet);
                log.info("Cabinet created: {}", cabinet.getLabel());
                return cabinet;
            });

        log.info("Filing statement in cabinet: {}", tenantCabinet.getLabel());
        cabinetService.addDocumentToCabinet(createdDocument.getId().intValue(), tenantCabinet.getId().intValue());
        log.info("Successfully filed statement in cabinet: {}", tenantCabinet.getLabel());
        return createdDocument;
    }
}
```

### Contributing

This client is a work in progress, and contributions are welcome. If you see a feature missing or an area for improvement, feel free to fork the repository, make your changes, and submit a merge request.

Note: The code examples provided above show how to configure and use the client in a Spring Boot application.