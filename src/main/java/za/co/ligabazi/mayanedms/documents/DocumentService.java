package za.co.ligabazi.mayanedms.documents;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import za.co.ligabazi.mayanedms.config.MayanaEDMSConfig;
import za.co.ligabazi.mayanedms.documents.domain.Document;

@Slf4j
@RequiredArgsConstructor
public class DocumentService {
    private final MayanaEDMSConfig config;
    private final RestTemplate restTemplate;

    private final String CONTEXT = "documents";

    public Document uploadDocument(File filePath, Long documentTypeId) {
        String url = String.format("%s/api/%s/%s/upload/", config.getHost(), config.getApiVer(), CONTEXT);
        log.debug("Uploading document: {}", filePath.getAbsolutePath());

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);
        headers.setBasicAuth(config.getUsername(), config.getPassword());

        MultiValueMap<String, Object> body = new LinkedMultiValueMap<>();

        // File part
        HttpHeaders filePartHeaders = new HttpHeaders();
        filePartHeaders.setContentType(MediaType.MULTIPART_FORM_DATA);
        HttpEntity<FileSystemResource> filePart = new HttpEntity<>(new FileSystemResource(filePath), filePartHeaders);
        body.add("file", filePart);

         Map<String, String> requestBody = new HashMap<>();
        //requestBody.put("document_type_id", uuid.toString());

        HttpHeaders jsonPartHeaders = new HttpHeaders();
        jsonPartHeaders.setContentType(MediaType.APPLICATION_JSON);
        //HttpEntity<String> jsonPart = new HttpEntity<>(beanJson, jsonPartHeaders);
        body.add("document_type_id", documentTypeId);

        HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity<>(body, headers);

        ResponseEntity<Document> response = restTemplate.postForEntity(url, requestEntity, Document.class);

        log.debug("Document uploaded: {}", response.getBody());
        return response.getBody();
    }

    public boolean deleteDocument(String documentId) {
        String url = String.format("%s/api/%s/%s/%s/", config.getHost(), config.getApiVer(), CONTEXT, documentId);
        log.debug("Deleting document with ID: {}", documentId);
    
        HttpHeaders headers = new HttpHeaders();
        headers.setBasicAuth(config.getUsername(), config.getPassword());
    
        HttpEntity<Void> requestEntity = new HttpEntity<>(headers);
    
        try {
            ResponseEntity<Void> response = restTemplate.exchange(url, HttpMethod.DELETE, requestEntity, Void.class);
    
            // Check if the deletion was successful based on the HTTP status code
            if (response.getStatusCode() == HttpStatus.OK || response.getStatusCode() == HttpStatus.NO_CONTENT) {
                log.debug("Document deleted successfully");
                return true;
            } else {
                log.error("Failed to delete document, status code: {}", response.getStatusCode());
                return false;
            }
        } catch (Exception e) {
            log.error("Error occurred during document deletion: {}", e);
            return false;
        }
    }

    public File downloadFile(String downloadUrl) throws IOException {
        HttpHeaders headers = new HttpHeaders();
        headers.setBasicAuth(this.config.getUsername(), this.config.getPassword());
        HttpEntity<Void> requestEntity = new HttpEntity(headers);
        ResponseEntity<Resource> response = this.restTemplate.exchange(downloadUrl, HttpMethod.GET, requestEntity, Resource.class, new Object[0]);
        if (response.getBody() != null) {
            // Generate a unique file name
            String fileName = "download_" + System.currentTimeMillis() + "_" + UUID.randomUUID().toString().substring(0, 8) + ".tmp";
            File tempFile = File.createTempFile(fileName, null);
            InputStream in = ((Resource)response.getBody()).getInputStream();

            try {
                FileOutputStream out = new FileOutputStream(tempFile);

                try {
                    byte[] buffer = new byte[4096];
                    int bytesRead;
                    while((bytesRead = in.read(buffer)) != -1) {
                        out.write(buffer, 0, bytesRead);
                    }
                } catch (Throwable var12) {
                    try {
                        out.close();
                    } catch (Throwable var11) {
                        var12.addSuppressed(var11);
                    }

                    throw var12;
                }

                out.close();
            } catch (Throwable var13) {
                if (in != null) {
                    try {
                        in.close();
                    } catch (Throwable var10) {
                        var13.addSuppressed(var10);
                    }
                }

                throw var13;
            }

            if (in != null) {
                in.close();
            }

            return tempFile;
        } else {
            throw new IOException("Failed to download the file: No content in response");
        }
    }
    
}
