package za.co.ligabazi.mayanedms.documents.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class DocumentFile {
    private Long id;
    private String download_url;
    private String document_url;
    private String filename;
    private Long document_id;
    private String mimetype;
}
