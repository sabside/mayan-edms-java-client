package za.co.ligabazi.mayanedms.documents.domain;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;
import za.co.ligabazi.mayanedms.documenttype.domain.DocumentType;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class Document {
    private Long id;
    private String label;
    private DocumentType document_type;
    private Date datetime_created;
    private String description;
    private DocumentFile file_latest;
}
